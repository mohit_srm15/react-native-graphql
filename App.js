/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View} from 'react-native';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import ApolloClient from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {createHttpLink} from 'apollo-link-http';
import {ApolloProvider} from '@apollo/react-hooks';
import {onError} from 'apollo-link-error';

import {AppContainer} from './src/Router';
import store from './src/Store';
import {CONSTANTS} from './src/Constants';

const httpLink = createHttpLink({
  uri: `${CONSTANTS.GRAPH_QL_URL}/graphql`
});

const errorLink = onError(({graphQLErrors, networkError}) => {
  if (graphQLErrors && graphQLErrors.length > 0 && graphQLErrors[0].message) {
    alert(graphQLErrors[0].message);
  }
});

const client = new ApolloClient({
  link: errorLink.concat(httpLink),
  cache: new InMemoryCache()
});

function App() {

  return (
    <View style={{flex: 1}}>
      <ApolloProvider client={client} >
        <Provider store={store}>
          <AppContainer />
        </Provider>
      </ApolloProvider>
    </View>
  );
}

export default App;
