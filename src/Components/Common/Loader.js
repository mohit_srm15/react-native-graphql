import React from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';

import Color from '../../Styles/Color';

function Loader(props) {
  return props.loading ? (
    <View style={styles.loader}>
      <ActivityIndicator size={20} color={Color.gray03} />
    </View>
  ) : null;
}
const styles = StyleSheet.create({
  loader: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Loader;
