import React,{useEffect, useState} from 'react';
import {View, TouchableOpacity, FlatList} from 'react-native';
import {useLazyQuery} from '@apollo/react-hooks'
import {connect} from 'react-redux';
 
import Loader from '../Components/Common/Loader';
import {addJobDetail} from '../Actions/ActionCreator';
import store from '../Store';
import {FETCH_JOBS} from '../API/job';
import { BoldText, LightText } from './Common/StyledTexts';
import Color from '../Styles/Color';


function HomeComp(props){

    const [ jobList , setJobList ] = useState([]);
    const [getJobData, {data , loading}] = useLazyQuery(FETCH_JOBS , {
        fetchPolicy: 'no-cache'
    });

    useEffect(()=>{
        getJobData();
    }, [0]);

    useEffect(()=>{
        if(data && data.jobs && data.jobs.length){
            setJobList(data.jobs);
        }
    }, [data]);

    function handleViewJobDetail(job){
        store.dispatch(addJobDetail(job));
        props.navigation.navigate('JobProfile');
    }

    return(
        <View style={{
            flex: 1,
            paddingVertical: 10,
            paddingHorizontal: 20
        }}>
            <Loader loading={loading}  />
            <FlatList
                data={jobList}
                style={{flex: 1}}
                ListEmptyComponent={<BoldText>No data found</BoldText>}
                renderItem={({item , index})=>{
                    return(
                        <View style={{
                            borderWidth: 1,
                            marginVertical: 15,
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            borderRadius: 20,
                            backgroundColor: Color.gray06,
                            borderColor: Color.gray05,
                        }}>
                            <View>
                                <BoldText style={{
                                    fontFamily: 'Nunito',
                                    fontSize: 18
                                }}>{item.title}</BoldText>
                            </View>
                            <View style={{
                                marginVertical: 5
                            }}>
                                <LightText style={{
                                    fontSize: 16,
                                    fontFamily: 'Nunito'
                                }}>
                                    {`Location:  ${item.cities.length ? item.cities[0].name  : item.remotes.length ? 'Remote' : 'N/A'}`}
                                </LightText>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end'
                            }}>
                                <TouchableOpacity 
                                    onPress={()=> handleViewJobDetail(item) }
                                    style={{
                                        paddingTop: 5, 
                                        paddingHorizontal: 10 
                                        }}
                                    >
                                    <LightText style={
                                        {color: Color.headerColor,
                                        fontFamily: 'Nunito'
                                    }}>
                                        View Details
                                    </LightText>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                }}
            />
        </View>
    )
}

export default HomeComp;