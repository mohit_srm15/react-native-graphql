import React from 'react';
import {View} from 'react-native';

import {BoldText} from '../Components/Common/StyledTexts';
import HomeComp from '../Components/Home';
import Color from '../Styles/Color';

function Home(props) {
    return(
        <View style={{
            flex: 1
        }}>
            <View style={{
                flex: 0.08,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Color.gray05,
            }}>
                <BoldText>Up Learn Jobs</BoldText>
            </View>
            <View style={{
                flex: 1,
                backgroundColor: 'white'
            }}>
                <HomeComp navigation={props.navigation} />
            </View>
        </View>
    )
}

export default Home;
