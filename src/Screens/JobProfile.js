import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

import { BoldText, LightText } from '../Components/Common/StyledTexts';
import Color from '../Styles/Color';



function JobProfile(props){

    let {jobDetail} = props.state;

    return(
        <View style={{
            flex: 1
        }}>
            <View style={{
                flex: 0.08,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Color.gray05,
            }}>
                <View style={{
                    flexDirection: 'row'
                }}>
                    <View style={{
                        flexDirection: 'column',
                        flex: 0.2,
                    }}>
                        <TouchableOpacity 
                            onPress={()=> props.navigation.goBack(null)}
                            style={{
                                alignItems: 'center' 
                            }}
                            >
                            <Image 
                                source={require('../../assets/back.png')} 
                                style={{
                                    height: 25,
                                    width: 25,
                                    tintColor: Color.gray04
                                }} 
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        flex: 0.8
                    }}>
                        <BoldText>Job Detail</BoldText>
                    </View>
                </View>
            </View>
            <View style={{
                flex: 1
            }}>
                <View style={{
                    flexDirection: 'row',
                    padding: 20,
                    // justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <View style={{
                        flexDirection: 'column',
                        flex: 0.7
                    }}>
                        <BoldText style={{
                            color: Color.gray04,
                            fontSize: 18,
                            letterSpacing: 2
                        }}>
                            Title:
                        </BoldText>
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        flex: 1.3
                    }}>
                        <LightText style={{fontSize: 17}}>
                            {jobDetail && jobDetail.title ? jobDetail.title : 'N/A'}
                        </LightText>
                    </View>
                </View>
                <View style={{
                    flexDirection: 'row',
                    padding: 20,
                    alignItems: 'center'
                }}>
                    <View style={{
                        flexDirection: 'column',
                        flex: 0.7
                    }}>
                        <BoldText style={{
                            color: Color.gray04,
                            fontSize: 18,
                            letterSpacing: 2
                        }}>
                            Location:
                        </BoldText>
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        flex: 1.3
                    }}>
                        <LightText style={{fontSize: 17}}>
                            {jobDetail && jobDetail.cities.length ? `${jobDetail.cities[0].name}, ${jobDetail.cities[0].country ? jobDetail.cities[0].country.name : null }` : 'N/A'}
                        </LightText>
                    </View>
                </View>
                <View style={{
                    flexDirection: 'row',
                    padding: 20,
                    alignItems: 'center'
                }}>
                    <View style={{
                        flexDirection: 'column',
                        flex: 0.7
                    }}>
                        <BoldText style={{
                            color: Color.gray04,
                            fontSize: 18,
                            letterSpacing: 2
                        }}>
                            Remote:
                        </BoldText>
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        flex: 1.3
                    }}>
                        <LightText style={{fontSize: 17}}>
                            {jobDetail && jobDetail.remotes.length ? 'Yes' : 'No'}
                        </LightText>
                    </View>
                </View>
                <View style={{
                    flexDirection: 'row',
                    padding: 20,
                    alignItems: 'center'
                }}>
                    <View style={{
                        flexDirection: 'column',
                        flex: 0.7,
                    }}>
                        <BoldText style={{
                            color: Color.gray04,
                            fontSize: 18,
                            letterSpacing: 2
                        }}>
                            Apply URL:
                        </BoldText>
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        flex: 1.3
                    }}>
                        <LightText style={{fontSize: 17}}>
                            {jobDetail && jobDetail.applyUrl ? jobDetail.applyUrl : 'N/A'}
                        </LightText>
                    </View>
                </View> 
            </View>
        </View>
    )
}

const mapStateToProps = (state) => {
    return {state};
};

export default JobProfile = connect(mapStateToProps)(JobProfile);