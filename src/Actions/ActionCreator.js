import {ACTIONS} from './Action';

export function addJobDetail(payload) {
  return {
    type: ACTIONS.JOB_DETAIL,
    payload: payload,
  };
}
