import gql from 'graphql-tag';

export const FETCH_JOBS  = gql`
    query jobs{
        jobs{
            id,
            title,
            applyUrl,
            cities{
                name
                country{
                    name
                }
            }
            remotes{
                id,
                type,
                name
            }
        }
    }
`;