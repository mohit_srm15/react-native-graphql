import {ACTIONS} from '../Actions/Action';

const initialState = {
  jobDetail: {},
};

export function rootReducer(state = initialState, action) {
  if (action.type === ACTIONS.JOB_DETAIL) {
    state = {...state, jobDetail: action.payload};
  }
  return state;
}
