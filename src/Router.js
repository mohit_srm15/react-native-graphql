import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Splash from './Screens/Splash';
import Home from './Screens/Home';
import JobProfile from './Screens/JobProfile';

const appNavigations = createStackNavigator(
  {
    Splash: {screen: Splash},
    Home: {screen: Home},
    JobProfile: {screen: JobProfile}
  },
  {
    headerMode: 'none',
  },
);

export const AppContainer = createAppContainer(appNavigations);
